<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    public function clients(){
      return $this->belongTo('App\client');
    }
    public function types(){
      return $this->belongTo('App\type');
    }
    public function sends(){
      return $this->belongTo('App\send');
    }
}
