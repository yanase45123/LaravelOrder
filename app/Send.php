<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send extends Model
{
    public function letters(){
      return $this->hasMany('App\Letter');
    }
}
