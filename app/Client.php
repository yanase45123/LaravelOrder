<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function letters(){
      return $this->hasMany('App\Letter');
    }
    public function constracts(){
      return $this->hasMany('App\Constract');
    }
    public function constdetails(){
      return $this->hasMany('App\Constdetail');
    }
}
