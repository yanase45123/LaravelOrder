<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Constract;
use Illuminate\Http\Request;

class ConstractController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$constracts = Constract::orderBy('id', 'desc')->paginate(10);

		return view('constracts.index', compact('constracts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('constracts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$constract = new Constract();

		$constract->client_id = $request->input("client_id");
        $constract->name = $request->input("name");
        $constract->genchodate = $request->input("genchodate");
        $constract->etimateadate = $request->input("etimateadate");
        $constract->contractdate = $request->input("contractdate");
        $constract->finishdate = $request->input("finishdate");

		$constract->save();

		return redirect()->route('constracts.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$constract = Constract::findOrFail($id);

		return view('constracts.show', compact('constract'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$constract = Constract::findOrFail($id);

		return view('constracts.edit', compact('constract'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$constract = Constract::findOrFail($id);

		$constract->client_id = $request->input("client_id");
        $constract->name = $request->input("name");
        $constract->genchodate = $request->input("genchodate");
        $constract->etimateadate = $request->input("etimateadate");
        $constract->contractdate = $request->input("contractdate");
        $constract->finishdate = $request->input("finishdate");

		$constract->save();

		return redirect()->route('constracts.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$constract = Constract::findOrFail($id);
		$constract->delete();

		return redirect()->route('constracts.index')->with('message', 'Item deleted successfully.');
	}

}
