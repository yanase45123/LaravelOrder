<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clients = Client::orderBy('id', 'desc')->paginate(10);

		return view('clients.index', compact('clients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('clients.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$client = new Client();

		$client->name = $request->input("name");
        $client->name2 = $request->input("name2");
        $client->company = $request->input("company");
        $client->department = $request->input("department");
        $client->position = $request->input("position");
        $client->dear = $request->input("dear");
        $client->postal_num = $request->input("postal_num");
        $client->prefecture = $request->input("prefecture");
        $client->address1 = $request->input("address1");
        $client->address2 = $request->input("address2");
        $client->building = $request->input("building");
        $client->phone_num = $request->input("phone_num");
        $client->num_other = $request->input("num_other");
        $client->comment = $request->input("comment");
        $client->mobie_mail = $request->input("mobie_mail");

		$client->save();

		return redirect()->route('clients.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$client = Client::findOrFail($id);

		return view('clients.show', compact('client'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$client = Client::findOrFail($id);

		return view('clients.edit', compact('client'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$client = Client::findOrFail($id);

		$client->name = $request->input("name");
        $client->name2 = $request->input("name2");
        $client->company = $request->input("company");
        $client->department = $request->input("department");
        $client->position = $request->input("position");
        $client->dear = $request->input("dear");
        $client->postal_num = $request->input("postal_num");
        $client->prefecture = $request->input("prefecture");
        $client->address1 = $request->input("address1");
        $client->address2 = $request->input("address2");
        $client->building = $request->input("building");
        $client->phone_num = $request->input("phone_num");
        $client->num_other = $request->input("num_other");
        $client->comment = $request->input("comment");
        $client->mobie_mail = $request->input("mobie_mail");

		$client->save();

		return redirect()->route('clients.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$client = Client::findOrFail($id);
		$client->delete();

		return redirect()->route('clients.index')->with('message', 'Item deleted successfully.');
	}

}
