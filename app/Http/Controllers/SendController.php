<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Send;
use Illuminate\Http\Request;

class SendController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sends = Send::orderBy('id', 'desc')->paginate(10);

		return view('sends.index', compact('sends'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sends.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$send = new Send();

		$send->name = $request->input("name");

		$send->save();

		return redirect()->route('sends.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$send = Send::findOrFail($id);

		return view('sends.show', compact('send'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$send = Send::findOrFail($id);

		return view('sends.edit', compact('send'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$send = Send::findOrFail($id);

		$send->name = $request->input("name");

		$send->save();

		return redirect()->route('sends.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$send = Send::findOrFail($id);
		$send->delete();

		return redirect()->route('sends.index')->with('message', 'Item deleted successfully.');
	}

}
