<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Constdetail;
use Illuminate\Http\Request;

class ConstdetailController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$constdetails = Constdetail::orderBy('id', 'desc')->paginate(10);

		return view('constdetails.index', compact('constdetails'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('constdetails.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$constdetail = new Constdetail();

		$constdetail->constract_id = $request->input("constract_id");
        $constdetail->client_id = $request->input("client_id");
        $constdetail->construction_id = $request->input("construction_id");
        $constdetail->order_id = $request->input("order_id");

		$constdetail->save();

		return redirect()->route('constdetails.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$constdetail = Constdetail::findOrFail($id);

		return view('constdetails.show', compact('constdetail'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$constdetail = Constdetail::findOrFail($id);

		return view('constdetails.edit', compact('constdetail'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$constdetail = Constdetail::findOrFail($id);

		$constdetail->constract_id = $request->input("constract_id");
        $constdetail->client_id = $request->input("client_id");
        $constdetail->construction_id = $request->input("construction_id");
        $constdetail->order_id = $request->input("order_id");

		$constdetail->save();

		return redirect()->route('constdetails.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$constdetail = Constdetail::findOrFail($id);
		$constdetail->delete();

		return redirect()->route('constdetails.index')->with('message', 'Item deleted successfully.');
	}

}
