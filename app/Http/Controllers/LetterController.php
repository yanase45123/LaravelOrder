<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Letter;
use Illuminate\Http\Request;

class LetterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$letters = Letter::orderBy('id', 'desc')->paginate(10);

		return view('letters.index', compact('letters'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('letters.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$letter = new Letter();

		$letter->client_id = $request->input("client_id");
        $letter->year = $request->input("year");
        $letter->type_id = $request->input("type_id");
        $letter->send_id = $request->input("send_id");

		$letter->save();

		return redirect()->route('letters.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$letter = Letter::findOrFail($id);

		return view('letters.show', compact('letter'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$letter = Letter::findOrFail($id);

		return view('letters.edit', compact('letter'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$letter = Letter::findOrFail($id);

		$letter->client_id = $request->input("client_id");
        $letter->year = $request->input("year");
        $letter->type_id = $request->input("type_id");
        $letter->send_id = $request->input("send_id");

		$letter->save();

		return redirect()->route('letters.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$letter = Letter::findOrFail($id);
		$letter->delete();

		return redirect()->route('letters.index')->with('message', 'Item deleted successfully.');
	}

}
