<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Construction;
use Illuminate\Http\Request;

class ConstructionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$constructions = Construction::orderBy('id', 'desc')->paginate(10);

		return view('constructions.index', compact('constructions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('constructions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$construction = new Construction();

		$construction->name = $request->input("name");

		$construction->save();

		return redirect()->route('constructions.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$construction = Construction::findOrFail($id);

		return view('constructions.show', compact('construction'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$construction = Construction::findOrFail($id);

		return view('constructions.edit', compact('construction'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$construction = Construction::findOrFail($id);

		$construction->name = $request->input("name");

		$construction->save();

		return redirect()->route('constructions.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$construction = Construction::findOrFail($id);
		$construction->delete();

		return redirect()->route('constructions.index')->with('message', 'Item deleted successfully.');
	}

}
