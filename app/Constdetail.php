<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constdetail extends Model
{
    public function clients(){
      return $this->belongTo('App\client');
    }
    public function constracts(){
      return $this->belongTo('App\constract');
    }
    public function costructions(){
      return $this->belongTo('App\construction');
    }
    public function orders(){
      return $this->belongTo('App\order');
    }
}
