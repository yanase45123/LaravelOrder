<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constract extends Model
{
  public function clients(){
    return $this->belongTo('App\client');
  }
  public function constdetails(){
    return $this->hasMany('App\Constdetail');
  }
}
