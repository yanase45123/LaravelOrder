<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('constdetails', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('constract_id');
            $table->integer('client_id');
            $table->integer('construction_id');
            $table->integer('order_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('constdetails');
	}

}
