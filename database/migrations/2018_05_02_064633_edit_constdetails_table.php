<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditConstdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('constdetails', function (Blueprint $table) {
            // $table->integer('constract_id')->unsigned()->change();
            // $table->foreign('constract_id')
            //       ->references('id')->on('constracts')
            //       ->onDelete('cascade')->change();
            $table->integer('client_id')->unsigned()->change();
            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('cascade')->change();
            $table->integer('construction_id')->unsigned()->change();
            $table->foreign('construction_id')
                  ->references('id')->on('constructions')
                  ->onDelete('cascade')
                  ->change();
            $table->integer('order_id')->unsigned()->change();
            $table->foreign('order_id')
                  ->references('id')->on('orders')
                  ->onDelete('cascade')
                  ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('constdetails', function (Blueprint $table) {
            //
        });
    }
}
