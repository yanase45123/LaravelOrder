<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name2');
            $table->string('company');
            $table->string('department');
            $table->string('position');
            $table->string('dear');
            $table->string('postal_num');
            $table->string('prefecture');
            $table->string('address1');
            $table->string('address2');
            $table->string('building');
            $table->string('phone_num');
            $table->string('num_other');
            $table->string('comment');
            $table->string('mobie_mail');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
