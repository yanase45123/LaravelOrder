<?php

use App\Client;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['middleware' => 'auth',function(){//先にミドルウェアを通す事でログイン判断ができる
  $clients = Client::orderBy('id', 'desc')->paginate(10);

	return view('clients.index', compact('clients'));
}]);


Route::resource("orders","OrderController");//受注
Route::resource("clients","ClientController");//顧客
Route::resource("constructions","ConstructionController");//工事マスタ
Route::resource("constdetails","ConstdetailController");//工事詳細
Route::resource("letters","LetterController");//郵便物
Route::resource("constracts","ConstractController");//契約
Route::resource("types","TypeController");//種別
Route::resource("sends","SendController");//送付
Route::resource("sample/csv",'CsvController');//CSV出力

Auth::Routes();
