@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Letters / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('letters.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('client_id')) has-error @endif">
                       <label for="client_id-field">Client_id</label>
                    <input type="text" id="client_id-field" name="client_id" class="form-control" value="{{ old("client_id") }}"/>
                       @if($errors->has("client_id"))
                        <span class="help-block">{{ $errors->first("client_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('year')) has-error @endif">
                       <label for="year-field">Year</label>
                    <input type="text" id="year-field" name="year" class="form-control date-picker" value="{{ old("year") }}"/>
                       @if($errors->has("year"))
                        <span class="help-block">{{ $errors->first("year") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('type_id')) has-error @endif">
                       <label for="type_id-field">Type_id</label>
                    <input type="text" id="type_id-field" name="type_id" class="form-control" value="{{ old("type_id") }}"/>
                       @if($errors->has("type_id"))
                        <span class="help-block">{{ $errors->first("type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('send_id')) has-error @endif">
                       <label for="send_id-field">Send_id</label>
                    <input type="text" id="send_id-field" name="send_id" class="form-control" value="{{ old("send_id") }}"/>
                       @if($errors->has("send_id"))
                        <span class="help-block">{{ $errors->first("send_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('letters.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
  $.fn.datepicker.dates['ja'] = {
    days: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"],
    daysShort: ["日", "月", "火", "水", "木", "金", "土"],
    daysMin: ["日", "月", "火", "水", "木", "金", "土"],
    months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    today: "今日",
    format: "yyyy/mm/dd",
    titleFormat: "yyyy年mm月",
    clear: "クリア"
  };
    $('.date-picker').datepicker({
      format:'yyyy-mm-dd',
      language: 'ja'
    });
  </script>
@endsection
