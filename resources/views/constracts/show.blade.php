@extends('layout')
@section('header')
<div class="page-header">
        <h1>契約 / 詳細 #{{$constract->id}}</h1>
        <form action="{{ route('constracts.destroy', $constract->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('constracts.edit', $constract->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="client_id">CLIENT_ID</label>
                     <p class="form-control-static">{{$constract->client_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$constract->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="genchodate">GENCHODATE</label>
                     <p class="form-control-static">{{$constract->genchodate}}</p>
                </div>
                    <div class="form-group">
                     <label for="etimateadate">ETIMATEADATE</label>
                     <p class="form-control-static">{{$constract->etimateadate}}</p>
                </div>
                    <div class="form-group">
                     <label for="contractdate">CONTRACTDATE</label>
                     <p class="form-control-static">{{$constract->contractdate}}</p>
                </div>
                    <div class="form-group">
                     <label for="finishdate">FINISHDATE</label>
                     <p class="form-control-static">{{$constract->finishdate}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('constracts.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection
