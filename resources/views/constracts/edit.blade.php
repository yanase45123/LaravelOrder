@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> 契約 / 編集 #{{$constract->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('constracts.update', $constract->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('client_id')) has-error @endif">
                       <label for="client_id-field">Client_id</label>
                    <input type="text" id="client_id-field" name="client_id" class="form-control" value="{{ is_null(old("client_id")) ? $constract->client_id : old("client_id") }}"/>
                       @if($errors->has("client_id"))
                        <span class="help-block">{{ $errors->first("client_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label for="name-field">名前</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $constract->name : old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('genchodate')) has-error @endif">
                       <label for="genchodate-field">現調日</label>
                    <input type="text" id="genchodate-field" name="genchodate" class="form-control date-picker" value="{{ is_null(old("genchodate")) ? $constract->genchodate : old("genchodate") }}"/>
                       @if($errors->has("genchodate"))
                        <span class="help-block">{{ $errors->first("genchodate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('etimateadate')) has-error @endif">
                       <label for="etimateadate-field">見積日</label>
                    <input type="text" id="etimateadate-field" name="etimateadate" class="form-control date-picker" value="{{ is_null(old("etimateadate")) ? $constract->etimateadate : old("etimateadate") }}"/>
                       @if($errors->has("etimateadate"))
                        <span class="help-block">{{ $errors->first("etimateadate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('contractdate')) has-error @endif">
                       <label for="contractdate-field">契約日</label>
                    <input type="text" id="contractdate-field" name="contractdate" class="form-control date-picker" value="{{ is_null(old("contractdate")) ? $constract->contractdate : old("contractdate") }}"/>
                       @if($errors->has("contractdate"))
                        <span class="help-block">{{ $errors->first("contractdate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('finishdate')) has-error @endif">
                       <label for="finishdate-field">完了日</label>
                    <input type="text" id="finishdate-field" name="finishdate" class="form-control date-picker" value="{{ is_null(old("finishdate")) ? $constract->finishdate : old("finishdate") }}"/>
                       @if($errors->has("finishdate"))
                        <span class="help-block">{{ $errors->first("finishdate") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('constracts.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js" charset="utf-8"></script>
  <script>
  $.fn.datepicker.dates['ja'] = {
    days: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"],
    daysShort: ["日", "月", "火", "水", "木", "金", "土"],
    daysMin: ["日", "月", "火", "水", "木", "金", "土"],
    months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
    today: "今日",
    format: "yyyy/mm/dd",
    titleFormat: "yyyy年mm月",
    clear: "クリア"
  };
    $('.date-picker').datepicker({
      format:'yyyy-mm-dd',
      language: 'ja'
    });
  </script>
@endsection
