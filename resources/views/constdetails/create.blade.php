@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Constdetails / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('constdetails.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('constract_id')) has-error @endif">
                       <label for="constract_id-field">Constract_id</label>
                    <input type="text" id="constract_id-field" name="constract_id" class="form-control" value="{{ old("constract_id") }}"/>
                       @if($errors->has("constract_id"))
                        <span class="help-block">{{ $errors->first("constract_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('client_id')) has-error @endif">
                       <label for="client_id-field">Client_id</label>
                    <input type="text" id="client_id-field" name="client_id" class="form-control" value="{{ old("client_id") }}"/>
                       @if($errors->has("client_id"))
                        <span class="help-block">{{ $errors->first("client_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('construction_id')) has-error @endif">
                       <label for="construction_id-field">Construction_id</label>
                    <input type="text" id="construction_id-field" name="construction_id" class="form-control" value="{{ old("construction_id") }}"/>
                       @if($errors->has("construction_id"))
                        <span class="help-block">{{ $errors->first("construction_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('order_id')) has-error @endif">
                       <label for="order_id-field">Order_id</label>
                    <input type="text" id="order_id-field" name="order_id" class="form-control" value="{{ old("order_id") }}"/>
                       @if($errors->has("order_id"))
                        <span class="help-block">{{ $errors->first("order_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('constdetails.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
