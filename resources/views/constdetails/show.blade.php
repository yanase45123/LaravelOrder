@extends('layout')
@section('header')
<div class="page-header">
        <h1>Constdetails / Show #{{$constdetail->id}}</h1>
        <form action="{{ route('constdetails.destroy', $constdetail->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('constdetails.edit', $constdetail->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="constract_id">CONSTRACT_ID</label>
                     <p class="form-control-static">{{$constdetail->constract_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="client_id">CLIENT_ID</label>
                     <p class="form-control-static">{{$constdetail->client_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="construction_id">CONSTRUCTION_ID</label>
                     <p class="form-control-static">{{$constdetail->construction_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="order_id">ORDER_ID</label>
                     <p class="form-control-static">{{$constdetail->order_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('constdetails.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection