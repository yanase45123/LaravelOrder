<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Starter Template</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="starter-template.css" rel="stylesheet"> -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
</head>

<body>
<!--
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel Order</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ route('clients.index')}}">顧客一覧</a></li>
                    <li><a href="{{ route('constracts.index')}}">契約一覧</a></li>
                    <li><a href="#">CSV検索</a></li>
                    <li><a href="#">CSV一括登録</a></li>
                    <li><a href="#">テキストエリア入力</a></li>
                    <li><a href="#about">About</a></li>--><!-- デフォ -->
                    <!--<li><a href="#contact">Contact</a></li>--><!-- デフォ --><!--
                </ul>
            </div>--><!--/.nav-collapse -->
        <!--</div>
    </nav>
    <h1>ここからオリジナルメニュー</h1>-->
    <nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarEexample">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Laravel</a>
		</div>

		<div class="collapse navbar-collapse" id="navbarEexample">
			<ul class="nav navbar-nav">
				<li class="dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">顧客・契約<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ route('clients.index')}}">顧客一覧</a></li>
						<li><a href="{{ route('constracts.index')}}">契約一覧</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">CSV<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">検索</a></li>
						<li><a href="#">一括登録</a></li>
						<li><a href="#">顧客テキストエリア入力</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">マスターメンテナンス<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ route('orders.index')}}">受注</a></li>
						<li><a href="{{ route('constructions.index')}}">工事</a></li>
					</ul>
				</li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button">ユーザー管理<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a>ユーザー一覧</a></li>
            <li><a class="nav-link" href="{{ route('register') }}">{{ __('ユーザー登録') }}</a></li>
          </ul>
        </li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
			</ul>
		</div>
	</div>
</nav>



    <div class="container">
        @yield('header')
        @yield('content')
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    @yield('scripts')
</body>
</html>
