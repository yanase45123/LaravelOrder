@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> 顧客 / 編集 #{{$client->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('clients.update', $client->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $client->name : old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('name2')) has-error @endif">
                       <label for="name2-field">Name2</label>
                    <input type="text" id="name2-field" name="name2" class="form-control" value="{{ is_null(old("name2")) ? $client->name2 : old("name2") }}"/>
                       @if($errors->has("name2"))
                        <span class="help-block">{{ $errors->first("name2") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('company')) has-error @endif">
                       <label for="company-field">Company</label>
                    <input type="text" id="company-field" name="company" class="form-control" value="{{ is_null(old("company")) ? $client->company : old("company") }}"/>
                       @if($errors->has("company"))
                        <span class="help-block">{{ $errors->first("company") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('department')) has-error @endif">
                       <label for="department-field">Department</label>
                    <input type="text" id="department-field" name="department" class="form-control" value="{{ is_null(old("department")) ? $client->department : old("department") }}"/>
                       @if($errors->has("department"))
                        <span class="help-block">{{ $errors->first("department") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('position')) has-error @endif">
                       <label for="position-field">Position</label>
                    <input type="text" id="position-field" name="position" class="form-control" value="{{ is_null(old("position")) ? $client->position : old("position") }}"/>
                       @if($errors->has("position"))
                        <span class="help-block">{{ $errors->first("position") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('dear')) has-error @endif">
                       <label for="dear-field">Dear</label>
                    <input type="text" id="dear-field" name="dear" class="form-control" value="{{ is_null(old("dear")) ? $client->dear : old("dear") }}"/>
                       @if($errors->has("dear"))
                        <span class="help-block">{{ $errors->first("dear") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('postal_num')) has-error @endif">
                       <label for="postal_num-field">Postal_num</label>
                    <input type="text" id="postal_num-field" name="postal_num" class="form-control" value="{{ is_null(old("postal_num")) ? $client->postal_num : old("postal_num") }}"/>
                       @if($errors->has("postal_num"))
                        <span class="help-block">{{ $errors->first("postal_num") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('prefecture')) has-error @endif">
                       <label for="prefecture-field">Prefecture</label>
                    <input type="text" id="prefecture-field" name="prefecture" class="form-control" value="{{ is_null(old("prefecture")) ? $client->prefecture : old("prefecture") }}"/>
                       @if($errors->has("prefecture"))
                        <span class="help-block">{{ $errors->first("prefecture") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('address1')) has-error @endif">
                       <label for="address1-field">Address1</label>
                    <input type="text" id="address1-field" name="address1" class="form-control" value="{{ is_null(old("address1")) ? $client->address1 : old("address1") }}"/>
                       @if($errors->has("address1"))
                        <span class="help-block">{{ $errors->first("address1") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('address2')) has-error @endif">
                       <label for="address2-field">Address2</label>
                    <input type="text" id="address2-field" name="address2" class="form-control" value="{{ is_null(old("address2")) ? $client->address2 : old("address2") }}"/>
                       @if($errors->has("address2"))
                        <span class="help-block">{{ $errors->first("address2") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('building')) has-error @endif">
                       <label for="building-field">Building</label>
                    <input type="text" id="building-field" name="building" class="form-control" value="{{ is_null(old("building")) ? $client->building : old("building") }}"/>
                       @if($errors->has("building"))
                        <span class="help-block">{{ $errors->first("building") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('phone_num')) has-error @endif">
                       <label for="phone_num-field">Phone_num</label>
                    <input type="text" id="phone_num-field" name="phone_num" class="form-control" value="{{ is_null(old("phone_num")) ? $client->phone_num : old("phone_num") }}"/>
                       @if($errors->has("phone_num"))
                        <span class="help-block">{{ $errors->first("phone_num") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('num_other')) has-error @endif">
                       <label for="num_other-field">Num_other</label>
                    <input type="text" id="num_other-field" name="num_other" class="form-control" value="{{ is_null(old("num_other")) ? $client->num_other : old("num_other") }}"/>
                       @if($errors->has("num_other"))
                        <span class="help-block">{{ $errors->first("num_other") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('comment')) has-error @endif">
                       <label for="comment-field">Comment</label>
                    <input type="text" id="comment-field" name="comment" class="form-control" value="{{ is_null(old("comment")) ? $client->comment : old("comment") }}"/>
                       @if($errors->has("comment"))
                        <span class="help-block">{{ $errors->first("comment") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('mobie_mail')) has-error @endif">
                       <label for="mobie_mail-field">Mobie_mail</label>
                    <input type="text" id="mobie_mail-field" name="mobie_mail" class="form-control" value="{{ is_null(old("mobie_mail")) ? $client->mobie_mail : old("mobie_mail") }}"/>
                       @if($errors->has("mobie_mail"))
                        <span class="help-block">{{ $errors->first("mobie_mail") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('clients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
