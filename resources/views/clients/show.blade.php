@extends('layout')
@section('header')
<div class="page-header">
        <h1>顧客 / 詳細 #{{$client->id}}</h1>
        <form action="{{ route('clients.destroy', $client->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('clients.edit', $client->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$client->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="name2">NAME2</label>
                     <p class="form-control-static">{{$client->name2}}</p>
                </div>
                    <div class="form-group">
                     <label for="company">COMPANY</label>
                     <p class="form-control-static">{{$client->company}}</p>
                </div>
                    <div class="form-group">
                     <label for="department">DEPARTMENT</label>
                     <p class="form-control-static">{{$client->department}}</p>
                </div>
                    <div class="form-group">
                     <label for="position">POSITION</label>
                     <p class="form-control-static">{{$client->position}}</p>
                </div>
                    <div class="form-group">
                     <label for="dear">DEAR</label>
                     <p class="form-control-static">{{$client->dear}}</p>
                </div>
                    <div class="form-group">
                     <label for="postal_num">POSTAL_NUM</label>
                     <p class="form-control-static">{{$client->postal_num}}</p>
                </div>
                    <div class="form-group">
                     <label for="prefecture">PREFECTURE</label>
                     <p class="form-control-static">{{$client->prefecture}}</p>
                </div>
                    <div class="form-group">
                     <label for="address1">ADDRESS1</label>
                     <p class="form-control-static">{{$client->address1}}</p>
                </div>
                    <div class="form-group">
                     <label for="address2">ADDRESS2</label>
                     <p class="form-control-static">{{$client->address2}}</p>
                </div>
                    <div class="form-group">
                     <label for="building">BUILDING</label>
                     <p class="form-control-static">{{$client->building}}</p>
                </div>
                    <div class="form-group">
                     <label for="phone_num">PHONE_NUM</label>
                     <p class="form-control-static">{{$client->phone_num}}</p>
                </div>
                    <div class="form-group">
                     <label for="num_other">NUM_OTHER</label>
                     <p class="form-control-static">{{$client->num_other}}</p>
                </div>
                    <div class="form-group">
                     <label for="comment">COMMENT</label>
                     <p class="form-control-static">{{$client->comment}}</p>
                </div>
                    <div class="form-group">
                     <label for="mobie_mail">MOBIE_MAIL</label>
                     <p class="form-control-static">{{$client->mobie_mail}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('clients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

    <!--Letters_Table表示-->
    <div class="row">
      <h2>Letter_table</h2>
        <div class="col-md-12">
          <table>
            <tr>
              <th>client_id</th>
              <th>Year</th>
              <th>Type_id</th>
              <th>Send_id</th>
              <th>action</th>
            </tr>
            @foreach ($client->letters as $letter)
            <tr>
              <td>{{ $letter->client_id }}</td>
              <td>{{ $letter->year }}</td>
              <td>{{ $letter->type_id }}</td>
              <td>{{ $letter->send_id }}</td>
              <td>
                <a class="btn btn-xs btn-primary" href="{{ url('letters/' .$letter->id)}}" class="glyphicon glyphicon-eye-open">
                  view
                </a>
              </td>
            </tr>
            @endforeach

          </table>
            <a class="btn btn-link" href="{{ route('clients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>


    <!--Constdetails_Table表示-->
    <div class="row">
      <h2>Constdetails_table</h2>
        <div class="col-md-12">
          <table>
            <tr>
              <th>constract_id</th>
              <th>client_id</th>
              <th>construction</th>
              <th>order_id</th>
              <th>action</th>
            </tr>
            @foreach ($client->constdetails as $constdetail)
            <tr>
              <td>{{ $constdetail->constract_id }}</td>
              <td>{{ $constdetail->client_id }}</td>
              <td>{{ $constdetail->construction_id }}</td>
              <td>{{ $constdetail->order_id }}</td>
              <td>
                <a class="btn btn-xs btn-primary" href="{{ url('constdetails/' .$constdetail->id)}}" class="glyphicon glyphicon-eye-open">
                  view
                </a>
              </td>
            </tr>
            @endforeach
          </table>
            <a class="btn btn-link" href="{{ route('clients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection
